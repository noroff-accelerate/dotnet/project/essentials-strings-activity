﻿using System.Diagnostics;
using System.Text;

namespace Noroff.Samples.EssentialsStringsActivity
{
    internal class Program
    {
        static void Main(string[] args)
        {
            // String Immutability
            string code = "TOPSECRET";
            string modifiedCode = code.Replace("TOP", "CONFIDENTIAL");
            Console.WriteLine(ReferenceEquals(code, modifiedCode)); // False

            // String Interning
            string agentID1 = "Agent007";
            string agentID2 = new string("Agent007".ToCharArray());
            Console.WriteLine(ReferenceEquals(agentID1, agentID2)); // False
            agentID2 = string.Intern(agentID2);
            Console.WriteLine(ReferenceEquals(agentID1, agentID2)); // True

            // String Formatting
            string message = "The package will arrive at {0}.";
            string formattedMessage = String.Format(message, "0800 hours");
            Console.WriteLine(formattedMessage);

            // StringBuilder vs Concatenation
            StringBuilder sb = new StringBuilder();
            string concatenatedString = "";
            Stopwatch sw = Stopwatch.StartNew();
            for (int i = 0; i < 10000; i++)
            {
                sb.Append('a');
            }
            sw.Stop();
            long stringBuilderTime = sw.ElapsedMilliseconds;

            sw.Restart();
            for (int i = 0; i < 10000; i++)
            {
                concatenatedString += 'a';
            }
            sw.Stop();
            long concatenationTime = sw.ElapsedMilliseconds;

            Console.WriteLine($"StringBuilder time: {stringBuilderTime}ms, Concatenation time: {concatenationTime}ms");

            // Splitting and Substrings
            string encodedMessage = "RED;BLUE;GREEN";
            string[] colors = encodedMessage.Split(';');
            string secretColor = encodedMessage.Substring(4, 4);
            Console.WriteLine(secretColor); // BLUE

            // String Searching
            bool containsRed = encodedMessage.Contains("RED");
            int indexOfGreen = encodedMessage.IndexOf("GREEN");
            Console.WriteLine($"Contains RED: {containsRed}, Index of GREEN: {indexOfGreen}");

            // Modifying Strings
            string transformedMessage = encodedMessage.Replace("RED", "YELLOW").ToLower();
            Console.WriteLine(transformedMessage);
        }
    }
}