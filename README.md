# Secret Decoder - String Operations Activity

Explore and master string operations in C# by developing a secret decoder program for a spy agency. Your mission is to manipulate and analyze strings to decode secret messages.

## Tasks

1. **Code Immutability Check**
   - Initialize `code` with "TOPSECRET".
   - Attempt to change it using concatenation and replacement.
   - Use `ReferenceEquals` to check if the reference changes.

2. **Agent Identifiers - String Interning**
   - Create `agentID1` and `agentID2` identically, but differently initialized.
   - Use `String.Intern` and check if they point to the same memory.

3. **Encoded Messages - String Formatting**
   - Create a message template.
   - Use various formatting techniques to insert secret data.

4. **Rapid Message Assembly - StringBuilder vs Concatenation**
   - Measure the time for building strings using `StringBuilder` and regular concatenation with a `Stopwatch`.

5. **Decipher the Code - Splitting and Substrings**
   - Split a long encoded string into sub-strings.
   - Extract specific parts using `Substring`.

6. **Locate the Keyword - String Searching**
   - Use `IndexOf` and `Contains` to find keywords in a message.

7. **Code Transformation - Modifying Strings**
   - Use `Replace`, `ToLower`, `ToUpper`.
   - Show how these create new string instances.

## Objective

Through this engaging thematic activity, gain a practical and in-depth understanding of string manipulation in C#, focusing on immutability, performance, and memory considerations.

## Running the Program

Compile and run your C# program to see the results of your secret decoder in action. Observe the output at each step to understand the intricacies of string operations.

## Note

This activity is designed to provide hands-on experience with string operations in a fun and interactive way, enhancing your programming skills in C#.
